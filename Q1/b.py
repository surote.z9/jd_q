from bs4 import BeautifulSoup
import requests
URL = "https://www.set.or.th/set/nvdrbystock.do?language=th&country=TH"

res = requests.get(URL)
res.encoding = "utf-8"

HTML_SOUP = BeautifulSoup(res.text, 'html.parser')
SCOPE = HTML_SOUP.find_all('table')[0].find('tbody').find_all('tr')
# print(SCOPE)
# print('Symbol  Buy  Sell  Total  Net  %')
for stock in SCOPE:
    for det in stock.find_all('td'):
        print(det.text,end='  ')
    print()
